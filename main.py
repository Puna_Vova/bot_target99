import telebot
from telebot import types
import data
import requests
from PIL import Image
from io import BytesIO
from roboflow import Roboflow

bot = telebot.TeleBot('6804413212:AAG0rDzzMya-DlwBy7gk-L3UdEcfCQufAlU')

rf = Roboflow(api_key="wyhq6FG7N9mNDtlNhIKb")
project = rf.workspace().project("recyclables-liys1")
model = project.version(1).model


@bot.message_handler(commands=['start'])
def start(message):

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    btn1 = types.KeyboardButton("👋 Поздороваться")
    markup.add(btn1)
    bot.send_message(message.from_user.id, f"{data.hi_mes}", reply_markup=markup)

@bot.message_handler(content_types=['text'])
def get_text_messages(message):

    if message.text == '👋 Поздороваться':
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True) #создание новых кнопок
        markup.add(*data.buttons_start)
        bot.send_message(message.from_user.id, '❓ Куда что выбросить ❓', reply_markup=markup)


    elif message.text == 'Отходы стекла':
        bot.send_message(message.from_user.id, f'{data.glass_mes}', parse_mode='Markdown')
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        markup.add(*data.buttons_glass)
        bot.send_message(message.from_user.id, 'Выберите тип отходов стекла:', reply_markup=markup)

    elif message.text == 'Лампы':
        bot.send_message(message.from_user.id, f'{data.lamp_mes}', parse_mode='Markdown')
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        markup.add(*data.buttons_lamp)
        bot.send_message(message.from_user.id, 'Выберите тип лампы:', reply_markup=markup)

    elif message.text == 'Элементы питания':
        bot.send_message(message.from_user.id, f'{data.bat_mes}', parse_mode='Markdown')
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        markup.add(*data.buttons_bat)
        bot.send_message(message.from_user.id, 'Выберите элемент питания:', reply_markup=markup)

    elif message.text == 'Отработанные автомасла':
        bot.send_message(message.from_user.id, f'{data.oil_mes}', parse_mode='Markdown')

    elif message.text == 'Изношенные шины':
        bot.send_message(message.from_user.id, f'{data.wil_mes}', parse_mode='Markdown')
        bot.send_photo(chat_id=message.chat.id,photo= 'https://target99.by/upload/resize_cache/iblock/6f7/fvrdbck4crwp738neor9v96a34chir5p/280_310_2/шины%20старые.jpg')


    if message.text in data.list:
        info = data.list[message.text]
        bot.send_message(message.from_user.id, info['message'], parse_mode='MarkdownV2')
        bot.send_photo(chat_id=message.chat.id, photo=info['photo'])

    elif message.text == 'Назад' or message.text == 'нет':
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        markup.add(*data.buttons_start)
        bot.send_message(message.from_user.id, '❓ Куда что выбросить ❓', reply_markup=markup)

@bot.message_handler(content_types=['photo'])
def handle_image(message):
    # Получение информации о самом большом изображении

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)  # создание новых кнопо
    bot.send_message(message.from_user.id, "Пожалуйста подождите, идет обработка запроса", reply_markup=markup)
    photo = message.photo[-1].file_id

    # Загрузка изображения с использованием Telegram Bot API
    image_url = bot.get_file_url(photo)
    response = requests.get(image_url)
    image_data = BytesIO(response.content)

    # Обработка изображения
    image = Image.open(image_data)
    image.save("image.jpg", "JPEG")
    # Дополнительные действия с изображением, если требуется


    prediction = model.predict("image.jpg")
    json_response = prediction.json()
    class_value = json_response['predictions'][0]['predictions'][0]['class']






    russian_translation = data.dictionary.get(class_value)
    btn1 = types.KeyboardButton(russian_translation)
    btn2 = types.KeyboardButton("нет")
    markup.add(btn1)
    markup.add(btn2)

    print(class_value)

    bot.send_message(message.from_user.id, 'Верно ❓', reply_markup=markup)



bot.polling(none_stop=True, interval=0) #обязательная для работы бота часть


